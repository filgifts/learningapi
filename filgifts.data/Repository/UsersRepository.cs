﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using filgifts.data.Service;
using filgifts.data.Models;

namespace filgifts.data.Repository
{
    public class UsersRepository : EntityService<Users>
    {
        public bool Validate(string username, string password)
        {
            try 
            {
                //var user = _db.Fetch<Users>("select top 1 * from gapi_acct_main where kbpmail=@0 and kbppass=@1", username, password);
                var user = _db.Fetch<Users>("select * from ApiUsers where username=@0 and password=@1", username, password);
                if (user.Any())
                {
                    return true;
                }
                return false;
            }
            catch(Exception ex)
            {
                throw new Exception("Error on UsersRepository.Get:" + ex.Message);
            }         
        }
    }
}
