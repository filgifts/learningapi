﻿using filgifts.data.Models;
using filgifts.data.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace filgifts.data.Repository
{
    public class TokenRepository : EntityService<Token>
    {
        public Token Get(string tokenString)
        {
            try
            {
                //var user = _db.Fetch<Users>("select top 1 * from gapi_acct_main where kbpmail=@0 and kbppass=@1", username, password);
                var token = _db.SingleOrDefault<Token>("select * from Token where tokenstring=@0", tokenString);
                return token;
            }
            catch (Exception ex)
            {
                throw new Exception("Error on UsersRepository.Get:" + ex.Message);
            }
        }

        public void UpdateTokenDate(string tokenString)
        {
            _db.Execute("update Token set DateAccessed=@0 where TokenString=@1", DateTime.UtcNow, tokenString);
        }
    }
}
