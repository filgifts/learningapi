using filgifts.data.Models;
using filgifts.data.Service;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace filgifts.data.Repository
{
    public class ControlUserRepository : EntityService<ControlUser>
    {
        public ControlUser Get(string username, string password)
        {
            try
            {
                var user = _db.Fetch<ControlUser>("select top 1 * from ControlUser where Username=@0 and Password=@1", username, password);
                if (user.Any())
                {
                    return user.FirstOrDefault();
                }
                return null;
            } 
            catch (Exception ex)
            {
                throw new Exception("Error on UsersRepository.Get:" + ex.Message);
            }           

        }
    }   
        
}
     