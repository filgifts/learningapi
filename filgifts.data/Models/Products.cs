﻿using filgifts.data.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace filgifts.data.Models
{
    [FilgiftsDBMapping(TableName="gapi_items_main", PrimaryKey="gapi")]
    public class Products
    {
        public string gapi { get; set; }
        public string kbpbrand { get; set; }
    }
}
