using filgifts.data.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace filgifts.data.Models
{
    public class ControlUser
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public Enums.ControlUserTypes UserType { get; set; }
    }
}
