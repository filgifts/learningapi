﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace filgifts.data.Models
{
    public class ProductCatalog
    {
        public string description { get; set; }
        public string longdesc { get; set; }
        public string kbpcateg { get; set; }
        public string kbpmodel { get; set; }
        public decimal kbppesoprice { get; set; }
        public decimal pesoexchangerate { get; set; }
    }
}
