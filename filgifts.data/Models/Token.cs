﻿using filgifts.data.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace filgifts.data.Models
{
    public class Token
    {
        public int Id { get; set; }
        public string TokenString { get; set; }
        public string Username { get; set; }
        public DateTime DateAccessed { get; set; }
    }
}
