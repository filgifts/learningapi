﻿using filgifts.data.Attribute;
using PetaPoco;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace filgifts.data.Service
{
    public class EntityService<T>
    {
        public Database _db;
        private string _tableName;
        private string _primaryKey;

        public EntityService()
        {
            _db = new Database("filgiftsDB");
            var filgiftsMapping = (FilgiftsDBMapping)System.Attribute.GetCustomAttribute(typeof(T), typeof(FilgiftsDBMapping));
            _tableName = filgiftsMapping == null ? typeof(T).Name : filgiftsMapping.TableName;
            _primaryKey = filgiftsMapping == null ? "Id" : filgiftsMapping.PrimaryKey;
        }

        public IEnumerable<T> ListAll()
        {
            var select = string.Format("select * from {0}", _tableName);
            return _db.Query<T>(select);
        }

        public T Get(string Id)
        {
            return _db.Single<T>(Id);
        }

        public void Update(T obj)
        {
            _db.Update(obj);

        }

        public int Add(T obj)
        {
            var result = _db.Insert(_tableName, _primaryKey, obj);
            return (int)result;
        }

        public void Delete(int id)
        {
            _db.Delete<T>(id);
        }
    }
}
