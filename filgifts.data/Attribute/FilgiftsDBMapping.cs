﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace filgifts.data.Attribute
{
    class FilgiftsDBMapping : System.Attribute
    {
        public string TableName { get; set; }
        public string PrimaryKey { get; set; }
    }
}
