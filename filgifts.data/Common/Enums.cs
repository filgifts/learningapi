﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace filgifts.data.Common
{
    public class Enums
    {
        public enum UserTypes
        {
            Retail = 0,
            Control = 1
        }

        public enum ControlUserTypes
        {
            Admin = 100,
            Warehouse = 101,
            Accounting = 102
        }
    }
}
