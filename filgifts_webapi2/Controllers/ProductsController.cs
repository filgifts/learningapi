﻿using filgifts.data.Models;
using filgifts.data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;


namespace filgifts_webapi2.Controllers
{
    public class ProductsController : ApiController
    {
        private ProductsRepository _productsRepository;

        public ProductsController()
        {

            _productsRepository = new ProductsRepository();

        }

        //[HttpGet]      
        //public IEnumerable<Products> GetProducts() {
        //return _productsRepository.ListAll();
        //   }

        [HttpGet]
        public Products Get(string gapi)
        {
            Products product = _productsRepository.GetProducts(gapi);

            if (product == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            return product;

        }

        [HttpPut]
          public void Update(Products product)
          {

             _productsRepository.UpdateProducts(product);
         }
     
    }
    }
    

