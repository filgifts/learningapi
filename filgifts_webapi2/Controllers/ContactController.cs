﻿using filgifts.data.Repository;
using filgifts.data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;

namespace filgifts_webapi2.Controllers
{
    public class ContactController : ApiController
    {

        private ContactRepository _contactRepository;

        public ContactController()
        {
            _contactRepository = new ContactRepository();
        }

        [HttpGet]
        public IEnumerable<Contact> GetAllContact()
        {
            _contactRepository = new ContactRepository();
            //Return list of all employees
            return _contactRepository.ListAll();
        }

        [HttpPost]
        public int Insert(Contact contact)
        {

            var newContactId = _contactRepository.Add(contact);
            return newContactId;
        }

        [HttpDelete]
        public int Delete(int contact)
        {

            _contactRepository.Delete(contact);
            return contact;


        }

        [HttpPut]
        public void Update(Contact contact)
        {
            _contactRepository.Update(contact);
        }

    }
}

