﻿//using filgifts_webapi2.Models;
using filgifts.data.Repository;
using filgifts.data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;





namespace filgifts_webapi2.Controllers
{

    public  class EmployeeController : ApiController
    {
        private EmployeeRepository _employeeRepository;

        public EmployeeController()
        {
            _employeeRepository = new EmployeeRepository();
        }

        [HttpGet]
        public IEnumerable<Employee> GetAllEmployees()
        {
            _employeeRepository = new EmployeeRepository();
            //Return list of all employees
            return _employeeRepository.ListAll();  
        }

        [HttpPost]
        public int Insert(Employee employee)
        {
         
            var newEmployeeId = _employeeRepository.Add(employee);
            return newEmployeeId;
        }

        [HttpDelete]
        public int Delete(int employee)
        {
             
            _employeeRepository.Delete(employee);
            return employee;


        }

       [HttpPut]
        public void Update(Employee employee)
        {  
            _employeeRepository.Update(employee);
        }

    }

}