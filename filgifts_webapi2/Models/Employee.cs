﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace filgifts_webapi2.Models
{
    public class Employee
    {
        public int id { get; set; }
        public string fname { get; set; }
        public string mname { get; set; }
        public string lname { get; set; }
    }

    public class EmpRow
    {
		public int numOfRows { get; set; }
  
    }
}